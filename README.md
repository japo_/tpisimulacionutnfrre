# Framework
* Django 1.11.13
* Python 3.5.2
* Bootstrap 4.1

## Objetivo

Desarrollar un programa de computadora que sea capaz de generar las variables aleatorias según la función de distribución obtenida en la Actividad 1 

## Reglas

1. Desarrollar un programa de computadora capaz de generar las variables aleatorias que responda a la distribución de intervalo entre llegada de los clientes (IA)
2. Desarrollar un programa de computadora capaz de generar las variables aleatorias que responda a la distribución de los tiempos de atención a los clientes (TA)

## Setup

1. git clone https://gitlab.com/japo_/tpisimulacionutnfrre.git
2. cd tpisimulacionutnfrre/
3. python manage.py migrate
4. python manage.py createsuperuser
5. python manage.py makemigrations generador
6. python manage.py migrate generador
7. python manage.py runserver